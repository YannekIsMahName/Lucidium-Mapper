class_name VisualEventBS
extends AnimatableBody3D

# V3 JSON SETTINGS
@export var beat : float = 0

@export_range(0, 3) var x = 0
@export_range(0, 2) var y = 0
# END OPF V3 JSON SETTINGS

